import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { OrdersPlacedComponent } from './orders/orders-placed/orders-placed.component';
import { OrdersCreateComponent } from './orders/orders-create/orders-create.component';
import { LoginComponent } from './Auth/Login/login.component';
import { SignUpComponent } from './Auth/signup/signup.component';


const routes: Routes = [
  {path:'', component:OrdersPlacedComponent},
  {path:'create', component:OrdersCreateComponent},
  {path:'.edit/:postId', component:OrdersCreateComponent},
  {path:'login', component:LoginComponent},
  {path:'signup', component:SignUpComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule{}
