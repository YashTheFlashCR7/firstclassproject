import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../auth-service';

@Component({
  templateUrl: './login.component.html'
})

export class LoginComponent
{
  enteredUserNameError = 'Please enter a username in the correct form';
  enteredEmailError = 'Please enter an Email address in the correct form';
  enteredOrderError = 'Please enter an order of no more than 100 characters';
  enteredPasswordError = 'Please enter a password that contains lowercase letters, uppercase letters and at least one number';

  constructor (public authService:AuthService){}
    onLogin(form: NgForm)
{
if (form.invalid)
{
return;
}
this.authService.login(form.value.enteredEmail, form.value.enteredPassword, form.value.enteredUsername)
console.log(form.value)
}
}
