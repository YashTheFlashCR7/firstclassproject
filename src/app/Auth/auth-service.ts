import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';

import { AuthData } from './auth-data.model';

@Injectable({providedIn: 'root'})
export class AuthService
{
  private token : string;
  constructor(private http: HttpClient) {}

  getToken(){
    return this.token;
  }

  createUser (email:string, password:string, username:string)
  {
    const authData: AuthData = {email:email, password:password, username:username};
      return this.http.post('https://localhost:3000/api/user/signup', authData)
  }

  login(email:string, password:string, username:string)
  {
    const authData: AuthData = {email:email, password:password, username:username};
    this.http.post<{token : string}>('https://localhost:3000/api/user/login',authData)
    .subscribe(response => {
    console.log(response);
  })
}

}
