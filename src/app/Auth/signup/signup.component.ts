import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

import { AuthService } from "../auth-service";

@Component({
  templateUrl: '../signup/signup.component.html'
})

export class SignUpComponent {

  constructor(private AuthService: AuthService) {

  }

  enteredUserNameError = 'Please enter a username in the correct form';
  enteredEmailError = 'Please enter an Email address in the correct form';
  enteredOrderError = 'Please enter an order of no more than 100 characters';
  enteredPasswordError = 'Please enter a password that contains lowercase letters, uppercase letters and at least one number';
  isLoading = 'false';

  onSignup(form: NgForm) {
    console.log(form.value)
    this.AuthService.createUser(form.controls['enteredEmail'].value, form.controls['enteredPassword'].value, form.controls['enteredUserName'].value)
      .subscribe(data => {
        console.log(data)
      },
        error => {
          console.log(error)
        })

  }

}
