import { Component, OnInit, OnDestroy } from '@angular/core';
import { Order } from '../order.model';
import { OrdersService } from '../order.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-orders-placed',
  templateUrl: './orders-placed.component.html',
  styleUrls: ['./orders-placed.component.css']
})

export class OrdersPlacedComponent implements OnInit, OnDestroy
{
  orders: Order [] = [];

  constructor(public orderService: OrdersService) {}
  private ordersSubscription: Subscription;

  ngOnInit() {

    this.orderService.getOrders();
    this.ordersSubscription = this.orderService.getPostUpdateListener()
    .subscribe((orders: Order[]) =>
    {
      this.orders = orders;
    });

  }
  onDelete(orderID: string )
  {
    this.orderService.deleteOrder(orderID);
  }

  ngOnDestroy()
  {
    this.ordersSubscription.unsubscribe();
  }

}
