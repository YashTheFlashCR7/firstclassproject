import { Component, Sanitizer, SecurityContext } from '@angular/core';
import { NgForm } from '@angular/forms';
import { OrdersService } from '../order.service';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-orders-create',
  templateUrl: './orders-create.component.html',
  styleUrls: ['./orders-create.component.css']
})
export class OrdersCreateComponent
{
  enteredUserNameError = 'Please enter a username in the correct form';
  enteredEmailError = 'Please enter an Email address in the correct form';
  enteredOrderError = '<b>Please enter an order of no more than 100 characters</b>';
  output:string;

  constructor(public OrderService:OrdersService, public route : ActivatedRoute, protected sanitizer: DomSanitizer){}
  onAddOrder(Orderform: NgForm)
  {

    if (Orderform.invalid)
    {
      return;
    }
    this.OrderService.addOrders(Orderform.value.enteredUserName, Orderform.value.enteredEmail, Orderform.value.enteredOrder);
    this.output = (this.sanitizer.sanitize(SecurityContext.HTML, Orderform.value.enteredOrder));
    Orderform.resetForm();
  }
}

