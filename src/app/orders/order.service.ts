import { Order } from './order.model';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
//import { HttpClient } from 'selenium-webdriver/http';

@Injectable({ providedIn: 'root' })

export class OrdersService{

private orders: Order [] = [];
private updatedOrders = new Subject<Order[]>();
  ordersSubscription: any;

getOrders() {

    this.http.get<{message: string, orders: any}>('https://localhost:3000/api/orders')
    .pipe(map((orderData)=>
    {
      return orderData.orders.map(order=>{
      return {
        userName: order.userName,
        Email: order.Email,
        PlacedOrder: order.PlacedOrder,
        id: order._id
      };
    });
  }))
  .subscribe((changedOrders)=>
  {
    this.orders = changedOrders;
    this.updatedOrders.next([...this.orders]);
  });
}

getPostUpdateListener(){
  return this.updatedOrders.asObservable();
}

addOrders(userName: string, Email: string, PlacedOrder: string)
{
const order: Order = {id:null, userName: userName, Email: Email, PlacedOrder: PlacedOrder};
this.http.post<{message: string, orderId: string} >('https://localhost:3000/api/orders', order)
.subscribe((responseOrderData)=>{
  console.log(responseOrderData.message);
  const id = responseOrderData.orderId;
  order.id = id;
  this.orders.push(order);
  this.updatedOrders.next([...this.orders]);
});
}

deleteOrder(orderID: string){
  this.http.delete('https://localhost:3000/api/orders/' + orderID)
  .subscribe(() =>
  {
    const updatedOrdersDel = this.orders.filter(order =>order.id!== orderID);
    this.orders= updatedOrdersDel;
    this.updatedOrders.next([...this.orders]);
    console.log('Order Deleted');
  });
}


constructor(private http: HttpClient) {}
}


