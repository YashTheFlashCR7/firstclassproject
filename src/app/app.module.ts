import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
//import { RouterModule } from '@angular/router';
import { MatInputModule, MatCardModule, MatButtonModule, MatExpansionModule, MatDialogModule } from '@angular/material';
import { OrdersCreateComponent } from './orders/orders-create/orders-create.component';
import { OrdersPlacedComponent } from './orders/orders-placed/orders-placed.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginComponent } from './Auth/Login/login.component';
import { ErrorComponent } from './Auth/error/error.component';
import { SignUpComponent } from './Auth/signup/signup.component';
import { AuthInterceptor } from './Auth/auth-interceptor';
import { ErrorInterceptor } from './error.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    OrdersCreateComponent,
    OrdersPlacedComponent,
    LoginComponent,
    SignUpComponent,
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatExpansionModule,
    HttpClientModule,
    MatDialogModule
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi:true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi:true}],
  bootstrap: [AppComponent],
  entryComponents: [ErrorComponent]
})
export class AppModule { }
