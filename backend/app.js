const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const fs = require('fs');
const orderRoutes = require('../backend/routes/order');
const userRoutes = require('../backend/routes/user');
// index.js
const cert = fs.readFileSync('./keys/certificate.pem');
const options = {
  server: { sslCA: cert}};

const app = express();


mongoose.connect("mongodb+srv://Jason08:Password1@cluster0-elb09.mongodb.net/test?retryWrites=true")
.then(() =>
{
  console.log("Connected to DB successfully! YAY")
})
.catch(()=>
{
console.log('Apparently not !!!')
});

app.use(bodyParser.json());

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader("Access-Control-Allow-Methods",
    "GET, POST, PATCH, PUT, DELETE, OPTIONS"
  );
  next();
});

app.use("/api/orders", orderRoutes.order)
app.use("/api/user", userRoutes.user)

module.exports = app;
